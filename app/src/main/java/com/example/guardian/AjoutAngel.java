package com.example.guardian;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class AjoutAngel extends AppCompatActivity {

    private TextView mNomText;
    private EditText mNomInput;
    private TextView mNumText;
    private EditText mNumInput;
    private TextView mMailText;
    private EditText mMailInput;
    private TextView mAdresseText;
    private EditText mAdresseInput;
    private Button mAjouterBtn;
    private ListView mListView;
    private Button contact;
    private final int REQUEST_CODE=99;
    private DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_angel);

        mNomText = findViewById(R.id.activity_ajout_nom_txt);
        mNomInput = findViewById(R.id.activity_ajout_nom_input);
        mNumText = findViewById(R.id.activity_ajout_num_text);
        mNumInput = findViewById(R.id.activity_ajout_num_input);
        mAdresseText =findViewById(R.id.activity_ajout_adresse_text);
        mAdresseInput = findViewById(R.id.activity_ajout_adresse_input);
        mMailText = findViewById(R.id.activity_ajout_mail_text);
        mMailInput = findViewById(R.id.activity_ajout_mail_input);
        mAjouterBtn = findViewById(R.id.activity_ajout_ajouter_btn);
        mListView = (ListView) findViewById(R.id.ListView);
        ArrayList<String> listAngel = new ArrayList<String>();

        db = new DBHelper(this);

        mAjouterBtn.setEnabled(false);

        contact = findViewById(R.id.Contact);
        mNomInput=(EditText) findViewById(R.id.activity_ajout_nom_input);
        mNumInput=(EditText) findViewById(R.id.activity_ajout_num_input);

        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
        mAjouterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor res = db.getAngels();
                String nameTXT = mNomInput.getText().toString();
                String numTXT = mNumInput.getText().toString();
                String mailTXT = mMailInput.getText().toString();
                String adressTXT = mAdresseInput.getText().toString();

                    boolean checkAdd = db.addAngel(nameTXT, numTXT,mailTXT, adressTXT);
                    if (checkAdd==true){
                        Toast.makeText(AjoutAngel.this, "Ange ajouté!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AjoutAngel.this, ListeAngel.class);
                        startActivity(intent);
                    }
                    else
                        Toast.makeText(AjoutAngel.this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
            }
        });

        mNumInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                   mAjouterBtn.setEnabled(s.toString().length()==10);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case (REQUEST_CODE):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = getContentResolver().query
                            (contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String contactId = c.getString(c.getColumnIndex
                                (ContactsContract.Contacts._ID));
                        String hasNumber = c.getString(c.getColumnIndex
                                (ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        String num = "";
                        String name = "";
                        if (Integer.valueOf(hasNumber) == 1) {
                            Cursor numbers = getContentResolver().query
                                    (ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                            while (numbers.moveToNext()) {
                                num = numbers.getString(numbers.getColumnIndex
                                        (ContactsContract.CommonDataKinds.Phone.NUMBER));
                                name = numbers.getString(numbers.getColumnIndex
                                        (ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                                mNomInput.setText(name);
                                mNumInput.setText(num);
                            }
                        }
                    }
                    break;
                }
        }
    }
}
