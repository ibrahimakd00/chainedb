package com.example.guardian;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button lancer_alert = findViewById(R.id.lancer_alert);
        lancer_alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent alert = new Intent(MainActivity.this, Alerte.class);
                startActivity(alert);
            }
        });

        Button ajout_angel = findViewById(R.id.ajout_angel);
        ajout_angel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ajout_angel = new Intent(MainActivity.this, AjoutAngel.class);
                startActivity(ajout_angel);
            }
        });

        Button angel = findViewById(R.id.list_angels);
        angel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent angel = new Intent(MainActivity.this, ListeAngel.class);
                startActivity(angel);
            }
        });
    }

    @Override
    public void onClick(View v) {

    }
}