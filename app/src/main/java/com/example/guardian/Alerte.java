package com.example.guardian;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.util.List;

public class Alerte extends AppCompatActivity implements LocationListener, View.OnClickListener {

    private LocationManager lManager;
    private Location location;
    private String choix_source = " ";
    //private TextView tvClientMsg;
    //private TextView tvServerMessage;
    private Button btn;
    //private TextView tvname;
    //private TextView tvphone;
    //private TextView tvmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alerte);
        Button boutonEnvoie = (Button) findViewById(R.id.envoyer);
        final EditText numero = (EditText) findViewById(R.id.numero);
        final EditText message = (EditText) findViewById(R.id.message);
        boutonEnvoie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = numero.getText().toString();
                String msg = message.getText().toString();
                Geocoder geo = new Geocoder(Alerte.this);
                List<Address> adresses = null;
                try{
                    adresses = geo.getFromLocation(location.getLatitude(), location.getLongitude(),1);
                }
                catch (IOException e){
                    e.printStackTrace();
                }
                Address adresse = adresses.get(0);

                if(num.length() >= 4 && msg.length() >0){
                    SmsManager.getDefault().sendTextMessage(num,null,msg + "\n" + "mes coordonnées sont:" +adresse.getAddressLine(0),null,null);
                    numero.setText(" ");
                    message.setText(" ");
                }
                else{
                    Toast.makeText(Alerte.this, "Entrer le texte de votre Alerte et/ou le numéro de l'Angel", Toast.LENGTH_SHORT).show();
                }
            }
        });

        lManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        reinitialisationEcran();

        findViewById(R.id.choix_source).setOnClickListener(this);
        findViewById(R.id.obtenir_position).setOnClickListener(this);
        findViewById(R.id.afficherAdresse).setOnClickListener(this);
        
        btn = findViewById(R.id.btn);
        //tvname = findViewById(R.id.tvname);
        //tvphone = findViewById(R.id.tvphone);
        //tvmail = findViewById(R.id.tvmail);

        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    public void onLocationChanged(Location location){
        Log.i("géolocalisation", "La position a changé");
        setProgressBarIndeterminateVisibility(false);
        findViewById(R.id.afficherAdresse).setEnabled(true);
        this.location = location;
        afficherLocation();
        lManager.removeUpdates(this);
    }

    private void choisirSource(){
        reinitialisationEcran();
        List<String> providers = lManager.getProviders(true);
        final String[] sources = new String[providers.size()];
        int i = 0;
        for (String provider : providers){
            sources[i++] = provider;
        }
        new AlertDialog.Builder(Alerte.this).setItems(sources, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                findViewById(R.id.obtenir_position).setEnabled(true);
                choix_source = sources[which];
                setTitle(String.format("%s - %s", getString(R.string.app_name), choix_source));
            }
        }).create().show();
    }

    @SuppressLint("MissingPermission")
    private void obtenirPosition(){
        setProgressBarIndeterminateVisibility(true);
        lManager.requestLocationUpdates(choix_source, 60000, 0, this);
    }

    private void afficherLocation() {
        ((TextView) findViewById(R.id.latitude)).setText(String.valueOf(location.getLatitude()));
        ((TextView) findViewById(R.id.longitude)).setText(String.valueOf(location.getLongitude()));
        ((TextView) findViewById(R.id.altitude)).setText(String.valueOf(location.getAltitude()));
    }

    private void reinitialisationEcran() {
        ((TextView) findViewById(R.id.latitude)).setText("0.0");
        ((TextView) findViewById(R.id.longitude)).setText("0.0");
        ((TextView) findViewById(R.id.altitude)).setText("0.0");
        ((TextView) findViewById(R.id.adresse)).setText("");

        findViewById(R.id.obtenir_position).setEnabled(false);
        findViewById(R.id.afficherAdresse).setEnabled(false);
    }

    private void afficherAdresse(){
        setProgressBarIndeterminateVisibility(true);
        Geocoder geo = new Geocoder(Alerte.this);
        try{
            List<Address> adresses = geo.getFromLocation(location.getLatitude(),location.getLongitude(),1);
            if(adresses != null && adresses.size() == 1){
                Address adresse = adresses.get(0);
                ((TextView)findViewById(R.id.adresse)).setText(String.format("%s, %s %s",
                        adresse.getAddressLine(0),
                        adresse.getPostalCode(),
                        adresse.getLocality()));
            }
            else {
                ((TextView) findViewById(R.id.adresse)).setText("L'adresse n'a pu être déterminée");
            }
        }
        catch (IOException e){
            e.printStackTrace();
            ((TextView)findViewById(R.id.adresse)).setText("L'adresse n'a pas pu être déterminée");
        }
        setProgressBarIndeterminateVisibility(false);
    }

    public void onProviderDisabled(String provider){
        Log.i("géolocalisation", "La source a été désactivé");
        Toast.makeText(Alerte.this, String.format("La source \"%s\" a été désactivée", provider), Toast.LENGTH_SHORT).show();
        lManager.removeUpdates(this);
        setProgressBarIndeterminateVisibility(false);
    }

    public void onProviderEnabled(String provider){
        Log.i("géolocalisation", "La source a été activé.");
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.choix_source:
                choisirSource();
                break;
            case R.id.obtenir_position:
                obtenirPosition();
                break;
            case R.id.afficherAdresse:
                afficherAdresse();
                break;
            default:
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri contactData = data.getData();
        Cursor c = getContentResolver().query(contactData, null, null, null, null);
        String phoneNumber="",emailAddress="";
        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
        if (resultCode == Activity.RESULT_OK){
            if (c.moveToFirst()){
                if(hasPhone.equalsIgnoreCase("1")) {
                    hasPhone = "true";
                }
                else{
                    hasPhone = "false";
                }
                if (Boolean.parseBoolean(hasPhone)){
                    Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + contactId, null, null);
                    while(phones.moveToNext()){
                        phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    }
                    phones.close();
                }
            }

            //tvname.setText("Name: " + name);
            //tvphone.setText("Phone: "+phoneNumber);
            Log.d("curs", name + "num" + phoneNumber);
        }
        c.close();
    }
}
